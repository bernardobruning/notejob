﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoteJob
{
    class Task
    {
        public Task(string[] words, string date)
        {
            this.Date = DateTime.Parse(date);
            this.Init = DateTime.Parse(String.Format("{0} {1}", date, words[0]));
            this.End = DateTime.Parse(String.Format("{0} {1}", date, words[1])); ;
            this.TaskID = words[2];
            this.Description = BuildDescription(words, 3);
        }

        private string BuildDescription(string[] words, int startKey)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = startKey; i < words.Length; i++)
            {
                if (i < words.Length-1)
                    builder.AppendFormat("{0} ", words[i]);
                else
                    builder.AppendFormat("{0}", words[i]);
            }

            return builder.ToString();
        }

        

        public DateTime Init { get; set; }

        public DateTime End { get; set; }

        public string TaskID { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }
    }
}
