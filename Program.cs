﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace NoteJob
{
    class Program
    {
        private string[] args;
        private RestClient client;

        public Program(string[] args)
        {
            this.args = args;
        }
        static void Main(string[] args)
        {
            Program program = new Program(args);
            program.Execute();
        }

        private void Execute()
        {
            this.client = new RestClient("http://jobmanager.itajai.quay.com.br/");
            this.client.FollowRedirects = false;
            this.Professional = args[0];
            this.ProfessionalPassword = args[1];
            this.Tasks = new List<Task>();
            FileStream file = File.Open(args[2], FileMode.Open);
            StreamReader reader = new StreamReader(file);
            String date = String.Empty;

            while (!reader.EndOfStream)
            {
                String line = reader.ReadLine();
                String[] words = line.Split(' ');

                if (String.IsNullOrEmpty(words[0])) continue;
                if (words[0].Contains("#")) continue;
                if (words[0] == "Date") { date = words[1]; continue; }
                if (words[0] == "Lunch") continue;

                CreateTask(words, date);

            }

            Console.WriteLine("Logando...");
            Login();

            foreach (var task in Tasks)
            {
                Console.WriteLine("Apontando tarefa {0}", task.TaskID);
                try
                {
                    SendTask(task);
                }
                catch (Exception)
                {
                    Console.WriteLine("Deu merda mano, corra para as colinas!");
                    break;
                }
            }

            Console.WriteLine("Apontamentos concluídos");
            Console.ReadKey();
        }

        private void Login()
        {
            IRestRequest request = new RestRequest("/Login");
            IRestResponse response = this.client.Execute(request);

            this.Cookies = response.Cookies;
            MatchCollection matches = Regex.Matches(response.Content, @"""([\w]|-)*""");

            request = new RestRequest("/Login", Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            request.AddParameter("UserName", this.Professional);
            request.AddParameter("Password", this.ProfessionalPassword);
            request.AddParameter("__RequestVerificationToken", matches[39].Value.Replace("\"", ""));
            

            foreach (var cookie in Cookies)
            {
                request.AddCookie(cookie.Name, cookie.Value);
            }

            response = this.client.Execute(request);
            if(response.StatusCode != System.Net.HttpStatusCode.Found) throw new Exception();

            foreach (var cookie in response.Cookies)
            {
                Cookies.Add(cookie);
            }
        }

        private void SendTask(Task task)
        {
            IRestRequest request = new RestRequest("api/apontamento/", Method.POST);
            request.AddParameter("Data", task.Date.ToString("o"));
            request.AddParameter("HoraInicio", task.Init.ToString("o"));
            request.AddParameter("HoraTermino", task.End.ToString("o"));
            request.AddParameter("IdWorkItemTFS", task.TaskID);
            request.AddParameter("Profissional", this.Professional);
            request.AddParameter("Descricao", task.Description);

            foreach (var cookie in Cookies)
            {
                request.AddCookie(cookie.Name, cookie.Value);
            }

            var result = client.Execute(request);
            if (result.StatusCode != HttpStatusCode.OK) throw new Exception();
        }

        private void CreateTask(string[] words, string date)
        {
            Tasks.Add(new Task(words, date));
        }

        public IList<Task> Tasks { get; set; }

        public string Professional { get; set; }

        public string ProfessionalPassword { get; set; }

        public string RequestToken { get; set; }

        public IList<RestResponseCookie> Cookies { get; set; }
    }
}
